package com.aybits.discoversaints;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.aybits.discoversaints.CustomizedListView.DownloadFileAndParseTask;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SearchListActivity extends Activity {

	static final String URL = "https://dl.dropboxusercontent.com/u/38102629/music.xml";
	// XML node keys
	static final String KEY_SONG = "song"; // parent node
	static final String KEY_ID = "id";
	static final String KEY_TITLE = "title";
	static final String KEY_ARTIST = "artist";
	static final String KEY_DURATION = "duration";
	static final String KEY_THUMB_URL = "thumb_url";
	static final String KEY_VIDEO_ID = "url_id";

	// List view
	private ListView lv;

	// Listview Adapter
	LazyAdapter adapter;

	// Search EditText
	EditText inputSearch;

	// ArrayList for Listview
	ArrayList<HashMap<String, String>> songsList;
	
	ArrayList<HashMap<String, String>> searchResults;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_activity);

		songsList = new ArrayList<HashMap<String, String>>();

		lv = (ListView) findViewById(R.id.list_view);
		lv.setItemsCanFocus(true);
		inputSearch = (EditText) findViewById(R.id.inputSearch);
		
		new DownloadFileAndParseTask().execute(URL);

		

		// Click event for single list row
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// Toast.makeText(getApplicationContext(),
				// "You selected item #: " + position + songsList.get(position +
				// 1).get(KEY_VIDEO_ID),Toast.LENGTH_SHORT).show();
				// songsList.get(position + 1);
				String VIDEO_ID = songsList.get(position).get(KEY_VIDEO_ID);
				int startTimeMillis = 0;
				boolean autoplay = false;
				boolean lightboxMode = true;
				Intent intent = YouTubeStandalonePlayer.createVideoIntent(
						SearchListActivity.this, DeveloperKey.DEVELOPER_KEY,
						VIDEO_ID, startTimeMillis, autoplay, lightboxMode);
				startActivity(intent);

			}
		});
		
		

		/**
		 * Enabling Search Filter
		 * */
		inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				//((LazyAdapter) SearchListActivity.this.adapter).getFilter().filter(cs);
				String inputSearchText = inputSearch.getText().toString();
				int textLength = inputSearchText.length();
				searchResults.clear();
				
				for(int i = 0; i < songsList.size(); i++){
					String title = songsList.get(i).get(KEY_TITLE).toString();
					if(textLength <= title.length()){
						if(inputSearchText.equalsIgnoreCase(title.substring(0, textLength))){
							searchResults.add(songsList.get(i));
							adapter = new LazyAdapter(SearchListActivity.this, searchResults);
							lv.setAdapter(adapter);
						}
					}
					adapter.notifyDataSetChanged();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

	}
	
	public class DownloadFileAndParseTask extends AsyncTask<String, Integer, ArrayList<HashMap<String, String>>> {

		ProgressDialog prog;
		Handler innerHandler = null;
		
		@Override
		protected void onPreExecute(){
			prog = new ProgressDialog(SearchListActivity.this);
			prog.setMessage("Loading....");
			prog.show();
		}
			
		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... arg0) {
			XMLParser parser = new XMLParser();
			String xml = parser.getXmlFromUrl(URL); // getting XML from URL
			Document doc = parser.getDomElement(xml); // getting DOM element

			NodeList nl = doc.getElementsByTagName(KEY_SONG);
			// looping through all song nodes <song>
			for (int i = 0; i < nl.getLength(); i++) {
				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();
				Element e = (Element) nl.item(i);
				// adding each child node to HashMap key => value
				map.put(KEY_ID, parser.getValue(e, KEY_ID));
				map.put(KEY_TITLE, parser.getValue(e, KEY_TITLE));
				map.put(KEY_ARTIST, parser.getValue(e, KEY_ARTIST));
				map.put(KEY_DURATION, parser.getValue(e, KEY_DURATION));
				map.put(KEY_THUMB_URL, parser.getValue(e, KEY_THUMB_URL));
				map.put(KEY_VIDEO_ID, parser.getValue(e, KEY_VIDEO_ID));

				// adding HashList to ArrayList
				songsList.add(map);
				
				

							}
			return songsList;
		}
		
		protected void onPostExecute(ArrayList<HashMap<String, String>> result){
			prog.dismiss();
			

			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					searchResults=new ArrayList<HashMap<String, String>>(songsList);
					// Getting adapter by passing xml data ArrayList
					adapter = new LazyAdapter(SearchListActivity.this, searchResults);
					//list.setAdapter(adapter);
					//list.setTextFilterEnabled(true);	
					
					// Adding items to listview
					//adapter = new ArrayAdapter<String>(SearchListActivity.this, R.layout.search_list_item,
					//		R.id.item_name, songsList);
					lv.setAdapter(adapter);
				}
			});
			
		}
	
	}


}
