package com.aybits.discoversaints;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeStandalonePlayer;

public class CustomizedListView extends Activity {
	// All static variables
	static final String URL = "http://37.139.31.138/discover_saints.xml";
	static final String KEY_SONG = "song"; // parent node
	static final String KEY_ID = "id";
	static final String KEY_TITLE = "title";
	static final String KEY_ARTIST = "artist";
	static final String KEY_DURATION = "duration";
	static final String KEY_THUMB_URL = "thumb_url";
	static final String KEY_VIDEO_ID = "url_id";

	public static final String PREFS_NAME = "MyPrefsFile";

	ListView list;
	LazyAdapter adapter;
	// ImageButton searchButton;
	ArrayList<HashMap<String, String>> songsList;
	SharedPreferences settings;
	String videoMode;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		settings = getSharedPreferences(PREFS_NAME, 0);
		videoMode = settings.getString("videoMode", "nativeapp");

		setContentView(R.layout.main);
		// searchButton = (ImageButton) findViewById(R.id.imageButton1);

		songsList = new ArrayList<HashMap<String, String>>();

		list = (ListView) findViewById(R.id.list);

		new DownloadFileAndParseTask().execute(URL);

		// Click event for single list row
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(getApplicationContext(), "Loading.....",
						Toast.LENGTH_LONG).show();
				String VIDEO_ID = songsList.get(position).get(KEY_VIDEO_ID);
				if (videoMode.equalsIgnoreCase("nativeapp")) {
					playViaNativeApp(VIDEO_ID);
				} else if (videoMode.equalsIgnoreCase("standaloneplayer")) {
					playViaStandAlonePlayer(VIDEO_ID);
				} else if (videoMode.equalsIgnoreCase("mobileapp")) {
					playViaMobilePage(VIDEO_ID);
				}

			}
		});

		// list.setLongClickable(true);
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// Toast.makeText(getApplicationContext(),
				// songsList.get(position).get(KEY_VIDEO_ID),
				// Toast.LENGTH_LONG).show();

				return false;
			}
		});

		/*
		 * searchButton.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent searchListIntent = new Intent(CustomizedListView.this,
		 * SearchListActivity.class); startActivity(searchListIntent); } });
		 */

	}

	public void playViaNativeApp(String id) {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:"
				+ id));
		startActivity(intent);
	}

	public void playViaMobilePage(String id) {
		startActivity(new Intent(Intent.ACTION_VIEW,
				Uri.parse("http://www.youtube.com/v/" + id)));
	}

	public void playViaStandAlonePlayer(String id) {
		int startTimeMillis = 0;
		boolean autoplay = false;
		boolean lightboxMode = true;
		Intent intent = YouTubeStandalonePlayer.createVideoIntent(
				CustomizedListView.this, DeveloperKey.DEVELOPER_KEY, id,
				startTimeMillis, autoplay, lightboxMode);
		startActivity(intent);
	}

	public void videoModeSettings() {
		final AlertDialog dialog;

		// Strings to Show In Dialog with Radio Buttons
		final CharSequence[] items = { " Stand alone player ",
				" Native youtube app ", " Mobile page " };

		// Creating and Building the Dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select The Player Mode");
		videoMode = settings.getString("videoMode", "nativeapp");
		int defaultValue = 1;
		if (videoMode.equalsIgnoreCase("nativeapp")) {
			defaultValue = 1;
		} else if (videoMode.equalsIgnoreCase("standaloneplayer")) {
			defaultValue = 0;
		} else if (videoMode.equalsIgnoreCase("mobileapp")) {
			defaultValue = 2;
		}
		builder.setSingleChoiceItems(items, defaultValue,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						SharedPreferences.Editor editor = settings.edit();

						switch (item) {

						case 0:
							editor.putString("videoMode", "standaloneplayer");
							editor.commit();
							break;
						case 1:
							editor.putString("videoMode", "nativeapp");
							editor.commit();
							break;
						case 2:
							editor.putString("videoMode", "mobileapp");
							editor.commit();
							break;

						}
						dialog.dismiss();
					}
				});
		dialog = builder.create();
		dialog.show();
	}

	public class DownloadFileAndParseTask extends
			AsyncTask<String, Integer, ArrayList<HashMap<String, String>>> {

		ProgressDialog prog;
		Handler innerHandler = null;

		@Override
		protected void onPreExecute() {
			prog = new ProgressDialog(CustomizedListView.this);
			prog.setMessage("Loading....");
			prog.show();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... arg0) {
			try {
				XMLParser parser = new XMLParser();
				String xml = parser.getXmlFromUrl(URL); // getting XML from URL
				Document doc = parser.getDomElement(xml); // getting DOM element

				NodeList nl = doc.getElementsByTagName(KEY_SONG);
				// looping through all song nodes <song>
				for (int i = 0; i < nl.getLength(); i++) {
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					Element e = (Element) nl.item(i);
					// adding each child node to HashMap key => value
					map.put(KEY_ID, parser.getValue(e, KEY_ID));
					map.put(KEY_TITLE, parser.getValue(e, KEY_TITLE));
					map.put(KEY_ARTIST, parser.getValue(e, KEY_ARTIST));
					map.put(KEY_DURATION, parser.getValue(e, KEY_DURATION));
					map.put(KEY_THUMB_URL, parser.getValue(e, KEY_THUMB_URL));
					map.put(KEY_VIDEO_ID, parser.getValue(e, KEY_VIDEO_ID));

					// adding HashList to ArrayList
					songsList.add(map);
				}
			} catch (Exception e) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getApplicationContext());

				// 2. Chain together various setter methods to set the dialog
				// characteristics
				builder.setMessage(
						"Error Occured! Please check your internet connection and restart the app.")
						.setTitle("Parsing error!");

				builder.setPositiveButton("Exit",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								finish();
								System.exit(1);
							}
						});
				
				// 3. Get the AlertDialog from create()
				AlertDialog dialog = builder.create();

				dialog.show();
			}
			return songsList;
		}

		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			prog.dismiss();

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// Getting adapter by passing xml data ArrayList
					adapter = new LazyAdapter(CustomizedListView.this,
							songsList);
					list.setAdapter(adapter);
					list.setTextFilterEnabled(true);
				}
			});

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		/*
		 * case R.id.search_menu_item: Intent searchIntent = new Intent(this,
		 * SearchListActivity.class); startActivity(searchIntent); return true;
		 */
		case R.id.video_mode:
			videoModeSettings();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
