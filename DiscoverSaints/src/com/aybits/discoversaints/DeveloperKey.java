// Copyright 2012 Google Inc. All Rights Reserved.

package com.aybits.discoversaints;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class DeveloperKey {

  /**
   * Please replace this with a valid API key which is enabled for the 
   * YouTube Data API v3 service. Go to the 
   * <a href="https://code.google.com/apis/console/">Google APIs Console</a> to
   * register a new developer key.
   */
  public static final String DEVELOPER_KEY = "AI39si5UwmZIsi-vAFKPvDufcZB6ErurdXoIdUl49z2FfL22H2SGVF4nRSWw5dv8pnbtJouoYQ0NqyyJIi6AQCWV5rfyPsa5uw";

}
